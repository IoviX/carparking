﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MenuScript : MonoBehaviour {


	public static int UnlockedLevels = 1;

	public Canvas exitPopup;
	public Canvas levelSelectPopup;
	public Button playButton;
	public Button exitButton;
	public Button backMenuButton;

	void Start (){
		exitPopup = exitPopup.GetComponent<Canvas>();
		exitPopup.enabled = false;

		levelSelectPopup = levelSelectPopup.GetComponent<Canvas>();
		levelSelectPopup.enabled = false;

		playButton = playButton.GetComponent<Button> ();
		exitButton = exitButton.GetComponent<Button> ();
		backMenuButton = backMenuButton.GetComponent<Button> ();
	}

	public void ExitPress() {
		exitPopup.enabled = true; 
		playButton.enabled = false; 
		exitButton.enabled = false;
	}

	public void NoPress() {
		exitPopup.enabled = false; 
		playButton.enabled = true; 
		exitButton.enabled = true;
	}

	public void SelectLevel (){
		levelSelectPopup.enabled = true;
	}


	public void BackToMenu (){
		levelSelectPopup.enabled = false;
	}
		
	//public void StartMission (){
	//	SceneManager.LoadScene (1);
	//}

	public void ExitGame () {
		Application.Quit(); 
	}
}
