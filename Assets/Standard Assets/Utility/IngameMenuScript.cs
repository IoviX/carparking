﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class IngameMenuScript : MonoBehaviour {

	public bool paused = false;

	public Canvas menu;
	public Button menuButton;

	void Start () {
		Time.timeScale = 1;
		menu = menu.GetComponent<Canvas>();
		menu.enabled = false;

		menuButton = menu.GetComponent<Button>();
	}
	
	void Update (){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			ToggleGame ();
		}
	}

	public void ToggleGame(){
		paused = !paused;
		if (paused) {
			Time.timeScale = 0;
			DisplayMenu ();
		} else {
			Time.timeScale = 1;
			HideMenu ();
		}
	}

	void DisplayMenu(){
		menu.enabled = true;
	}

	void HideMenu(){
		menu.enabled = false;
	}

	public void ExitGame (){
		Application.Quit(); 
	}

	public void AbortMission (){
		SceneManager.LoadScene (0);
	}

	public void WonMission (){
		MenuScript.UnlockedLevels++;
		AbortMission ();
	}
}
