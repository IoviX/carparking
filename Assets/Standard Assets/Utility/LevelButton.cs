﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour {

	public Button levelButton;
	public int level = 1;

	void Start () {
		levelButton = levelButton.GetComponent<Button>();

		if (level <= MenuScript.UnlockedLevels) {
			levelButton.interactable = true;
		} else {
			levelButton.interactable = false;
		}
	}

	public void startLevel(){
		SceneManager.LoadScene (level);
	}
}
