﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
	public int startingHealth = 100;                           
	public int currentHealth;                                  
	public Slider healthSlider;                                
	public Canvas lostPopup;
	public int minDmgTreshold = 12;
	public int maxTotalDmg = 20;
	public int baseDmg = 10;
	public float accRatio = 1f;
	public Transform car;

	//public int dmg;
	//public float acc;

	bool isDead;    

	void Start () {
		lostPopup = lostPopup.GetComponent<Canvas>();
		car = car.GetComponent<Transform>();
	}

	void OnTriggerEnter (Collider collision){
		if (collision.gameObject.tag != "Finish") {
			var rb = car.GetComponent<Rigidbody> ();
			float acc = rb.velocity.magnitude;
			int dmg = (int)(baseDmg + acc * accRatio);

			if (dmg > minDmgTreshold) {
				if (dmg > maxTotalDmg) {
					dmg = maxTotalDmg;
				}

				TakeDamage (dmg);
			}
		}
	}

	void Awake (){
		currentHealth = startingHealth;
	}

	public void TakeDamage (int amount){
		currentHealth -= amount;
		healthSlider.value = currentHealth;

		if(currentHealth <= 0 && !isDead){
			Death ();
		}
	}


	void Death (){
		isDead = true;
		lostPopup.enabled = true;
		Time.timeScale = 0;
	}       
}