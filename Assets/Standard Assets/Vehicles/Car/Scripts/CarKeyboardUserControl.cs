﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
	[RequireComponent(typeof (CarController))]
	public class CarKeyboardUserControl : MonoBehaviour
	{
		private CarController m_Car; // the car controller we want to use


		private void Awake(){
			// get the car controller
			m_Car = GetComponent<CarController>();
		}


		private void FixedUpdate(){
			float steering = 0f;
			float accel = 0f;
			float footbrake = 0f;
			float handbrake = 0f;

			if(Input.GetKey(KeyCode.W))
				accel = 1;
			else if (Input.GetKey (KeyCode.S))
				footbrake = -1;

			if (Input.GetKey (KeyCode.A)) 
				steering = -1;
			else if (Input.GetKey (KeyCode.D)) 
				steering = 1;

			if(Input.GetKey(KeyCode.Space))
				handbrake = 1;

			m_Car.Move(steering, accel, footbrake, handbrake);
		}
	}
}
