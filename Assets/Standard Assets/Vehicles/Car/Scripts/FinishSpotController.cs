﻿using UnityEngine;
using System.Collections;

public class FinishSpotController : MonoBehaviour {

	public Canvas winPopup;
	//public Canvas lostPopup;

	void Start () {
		winPopup = winPopup.GetComponent<Canvas>();
		winPopup.enabled = false;
	}

	void OnTriggerEnter (Collider collision){
		if (collision.gameObject.tag == "Player") {
			winPopup.enabled = true;
			Time.timeScale = 0;
		}
	}
}
